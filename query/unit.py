#! /usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import json

with open('ports.json') as f:
    data = json.load(f)
f.close()

with open('config.json') as f:
    datac = json.load(f)
f.close()

id_conf = int(datac['unit']['id'])
query_id = int(id_conf)
port_conf = int(data['unit'][query_id]['port'])


def dict_to_binary(the_dict):
    str = json.dumps(the_dict)
    binary = ' '.join(format(ord(letter), 'b') for letter in str)
    return binary

s = socket.socket()
host = socket.gethostname()
port = port_conf

s.bind((host, port))

s.listen(5)

while True:
   with open('config.json') as json_data:
       config = json.load(json_data)
   json_data.close()

   c, addr = s.accept()
   print ('Получил соединение от', addr)
   send_config = bytes(dict_to_binary(config), 'utf-8')
   c.send(send_config)
   c.close()
   print("re opened")
