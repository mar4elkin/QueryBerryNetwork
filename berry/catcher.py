#! /usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import json
from threading import Thread

super_array = []

with open('../query/ports.json') as f: #сделать db и импорт из бд
    data = json.load(f)
    for i in data:
        port_conf = data['unit']
f.close()

super_array.append(port_conf)

def binary_to_dict(the_binary):
    jsn = ''.join(chr(int(x, 2)) for x in the_binary.split())
    d = json.loads(jsn)
    return d

s = socket.socket()
host = socket.gethostname()
port = 12345

s.connect((host, port))
json_to_php = binary_to_dict(s.recv(1024))
super_array.append(json_to_php)
print(json.dumps(super_array))
s.close
